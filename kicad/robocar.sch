EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:custom
LIBS:switches
LIBS:relays
LIBS:Motor
LIBS:robocar-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L NodeMCU_1.0v3 U2
U 1 1 5975CCBD
P 3400 3500
F 0 "U2" H 3300 2850 60  0000 C CNN
F 1 "NodeMCU_1.0v3" H 3300 2850 60  0000 C CNN
F 2 "" H 3300 2850 60  0001 C CNN
F 3 "" H 3300 2850 60  0001 C CNN
	1    3400 3500
	1    0    0    -1  
$EndComp
$Comp
L RM50-xx11 RL1
U 1 1 5975D53A
P 6150 2600
F 0 "RL1" H 6600 2750 50  0000 L CNN
F 1 "5ND106-W1" H 6600 2650 50  0000 L CNN
F 2 "" H 6150 2600 50  0000 C CNN
F 3 "" H 6150 2600 50  0000 C CNN
	1    6150 2600
	1    0    0    1   
$EndComp
$Comp
L RM50-xx11 RL2
U 1 1 5975D59B
P 7400 3400
F 0 "RL2" H 7850 3550 50  0000 L CNN
F 1 "5ND106-W1" H 7850 3450 50  0000 L CNN
F 2 "" H 7400 3400 50  0000 C CNN
F 3 "" H 7400 3400 50  0000 C CNN
	1    7400 3400
	1    0    0    1   
$EndComp
$Comp
L PN2222A Q1
U 1 1 5975DD44
P 5850 3200
F 0 "Q1" H 6050 3275 50  0000 L CNN
F 1 "PN2222A" H 6050 3200 50  0000 L CNN
F 2 "" H 6050 3125 50  0000 L CIN
F 3 "" H 5850 3200 50  0000 L CNN
	1    5850 3200
	1    0    0    -1  
$EndComp
$Comp
L PN2222A Q2
U 1 1 5975DDD9
P 7100 4150
F 0 "Q2" H 7300 4225 50  0000 L CNN
F 1 "PN2222A" H 7300 4150 50  0000 L CNN
F 2 "" H 7300 4075 50  0000 L CIN
F 3 "" H 7100 4150 50  0000 L CNN
	1    7100 4150
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5975E038
P 5650 3450
F 0 "R2" V 5730 3450 50  0000 C CNN
F 1 "10k" V 5650 3450 50  0000 C CNN
F 2 "" V 5580 3450 50  0000 C CNN
F 3 "" H 5650 3450 50  0000 C CNN
	1    5650 3450
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5975E120
P 6900 4400
F 0 "R4" V 6980 4400 50  0000 C CNN
F 1 "10k" V 6900 4400 50  0000 C CNN
F 2 "" V 6830 4400 50  0000 C CNN
F 3 "" H 6900 4400 50  0000 C CNN
	1    6900 4400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5975E1B6
P 5400 3200
F 0 "R1" V 5480 3200 50  0000 C CNN
F 1 "1k" V 5400 3200 50  0000 C CNN
F 2 "" V 5330 3200 50  0000 C CNN
F 3 "" H 5400 3200 50  0000 C CNN
	1    5400 3200
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5975E240
P 6650 4150
F 0 "R3" V 6730 4150 50  0000 C CNN
F 1 "1k" V 6650 4150 50  0000 C CNN
F 2 "" V 6580 4150 50  0000 C CNN
F 3 "" H 6650 4150 50  0000 C CNN
	1    6650 4150
	0    1    1    0   
$EndComp
$Comp
L GND #PWR2
U 1 1 5975FC21
P 1300 4750
F 0 "#PWR2" H 1300 4500 50  0001 C CNN
F 1 "GND" H 1300 4600 50  0000 C CNN
F 2 "" H 1300 4750 50  0000 C CNN
F 3 "" H 1300 4750 50  0000 C CNN
	1    1300 4750
	1    0    0    -1  
$EndComp
$Comp
L D_ALT D1
U 1 1 5B28B0E3
P 5500 2600
F 0 "D1" H 5500 2700 50  0000 C CNN
F 1 "1N4148" H 5500 2500 50  0000 C CNN
F 2 "" H 5500 2600 50  0000 C CNN
F 3 "" H 5500 2600 50  0000 C CNN
	1    5500 2600
	0    1    1    0   
$EndComp
$Comp
L D_ALT D2
U 1 1 5B28B1DF
P 6800 3400
F 0 "D2" H 6800 3500 50  0000 C CNN
F 1 "1N4148" H 6800 3300 50  0000 C CNN
F 2 "" H 6800 3400 50  0000 C CNN
F 3 "" H 6800 3400 50  0000 C CNN
	1    6800 3400
	0    1    1    0   
$EndComp
$Comp
L Motor_DC M1
U 1 1 5B28B0DF
P 7600 2550
F 0 "M1" H 7700 2650 50  0000 L CNN
F 1 "Motor_DC" H 7700 2350 50  0000 L TNN
F 2 "" H 7600 2460 50  0001 C CNN
F 3 "" H 7600 2460 50  0001 C CNN
	1    7600 2550
	1    0    0    -1  
$EndComp
$Comp
L MCP1700-3302E_TO92 U1
U 1 1 5B28C508
P 1700 3300
F 0 "U1" H 1550 3175 50  0000 C CNN
F 1 "MCP1702" H 1700 3175 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 1700 3100 50  0001 C CIN
F 3 "" H 1700 3300 50  0001 C CNN
	1    1700 3300
	1    0    0    1   
$EndComp
Wire Wire Line
	4650 3200 5250 3200
Wire Wire Line
	5550 3200 5650 3200
Wire Wire Line
	5650 3200 5650 3300
Wire Wire Line
	5950 2900 5950 3000
Wire Wire Line
	4650 3300 5200 3300
Wire Wire Line
	5200 3300 5200 4150
Wire Wire Line
	6800 4150 6900 4150
Wire Wire Line
	6900 4150 6900 4250
Wire Wire Line
	7200 3700 7200 4000
Wire Bus Line
	1300 4750 8350 4750
Wire Wire Line
	2200 4150 2200 4750
Wire Wire Line
	5650 3600 5650 4750
Wire Wire Line
	6900 4550 6900 4750
Wire Wire Line
	6250 2900 6250 4750
Wire Wire Line
	7500 3700 7500 4750
Wire Wire Line
	5950 3400 5950 4750
Wire Wire Line
	7200 4350 7200 4750
Wire Bus Line
	1300 1900 8350 1900
Wire Wire Line
	5950 1900 5950 2300
Wire Wire Line
	7200 1950 7200 3100
Wire Wire Line
	5950 2950 5500 2950
Wire Wire Line
	5500 2950 5500 2750
Connection ~ 5950 2950
Wire Wire Line
	5950 2250 5500 2250
Wire Wire Line
	5500 2250 5500 2450
Connection ~ 5950 2250
Wire Wire Line
	7200 3750 6800 3750
Wire Wire Line
	6800 3750 6800 3550
Connection ~ 7200 3750
Wire Wire Line
	7200 3050 6800 3050
Wire Wire Line
	6800 3050 6800 3250
Connection ~ 7200 3050
Wire Wire Line
	5200 4150 6500 4150
Connection ~ 7200 1900
Connection ~ 5950 1900
Connection ~ 5650 4750
Connection ~ 5950 4750
Connection ~ 6250 4750
Connection ~ 6900 4750
Connection ~ 7200 4750
Connection ~ 7500 4750
Connection ~ 2200 4750
Wire Wire Line
	6450 2900 6450 3850
Wire Wire Line
	6450 3850 7700 3850
Wire Wire Line
	7700 3850 7700 3700
Wire Wire Line
	6450 2950 7100 2950
Wire Wire Line
	7100 2950 7100 1900
Connection ~ 6450 2950
Wire Wire Line
	6350 2250 6350 2300
Wire Wire Line
	7600 3100 7600 2850
Wire Wire Line
	6350 2250 7600 2250
Wire Wire Line
	7600 2250 7600 2350
Connection ~ 7100 1900
Wire Wire Line
	2000 3300 2200 3300
Wire Wire Line
	1700 3600 1700 4750
Wire Wire Line
	1400 1900 1400 3600
$Comp
L CP C1
U 1 1 5B28CFB9
P 1400 3750
F 0 "C1" H 1425 3850 50  0000 L CNN
F 1 "330µ" H 1425 3650 50  0000 L CNN
F 2 "" H 1438 3600 50  0001 C CNN
F 3 "" H 1400 3750 50  0001 C CNN
	1    1400 3750
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 5B28D2C4
P 2000 3750
F 0 "C2" H 2025 3850 50  0000 L CNN
F 1 "330µ" H 2025 3650 50  0000 L CNN
F 2 "" H 2038 3600 50  0001 C CNN
F 3 "" H 2000 3750 50  0001 C CNN
	1    2000 3750
	1    0    0    -1  
$EndComp
Connection ~ 1400 3300
Wire Wire Line
	1400 3900 1400 4750
Wire Wire Line
	2000 3900 2000 4750
Wire Wire Line
	2000 3600 2000 3300
Connection ~ 1400 4750
Connection ~ 1700 4750
Connection ~ 2000 4750
Connection ~ 2000 3300
Connection ~ 1400 1900
$Comp
L R R5
U 1 1 5B28D2DC
P 4950 3750
F 0 "R5" V 5030 3750 50  0000 C CNN
F 1 "1k" V 4950 3750 50  0000 C CNN
F 2 "" V 4880 3750 50  0001 C CNN
F 3 "" H 4950 3750 50  0001 C CNN
	1    4950 3750
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST SW1
U 1 1 5B28D329
P 4950 4400
F 0 "SW1" H 4950 4525 50  0000 C CNN
F 1 "SW_SPST" H 4950 4300 50  0000 C CNN
F 2 "" H 4950 4400 50  0000 C CNN
F 3 "" H 4950 4400 50  0000 C CNN
	1    4950 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 3600 4950 3400
Wire Wire Line
	4950 3400 4650 3400
Wire Wire Line
	4950 3900 4950 4200
Wire Wire Line
	4950 4600 4950 4750
Connection ~ 4950 4750
$Comp
L +BATT #PWR1
U 1 1 5B28DCD0
P 1300 1900
F 0 "#PWR1" H 1300 1750 50  0001 C CNN
F 1 "+BATT" H 1300 2040 50  0000 C CNN
F 2 "" H 1300 1900 50  0001 C CNN
F 3 "" H 1300 1900 50  0001 C CNN
	1    1300 1900
	1    0    0    -1  
$EndComp
Connection ~ 1300 1900
$EndSCHEMATC
