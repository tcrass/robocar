//#include "Arduino.h"
#include <ESP8266WiFi.h>

const unsigned short STATUS_READY = 0;
const unsigned short STATUS_FORWARD = 1;
const unsigned short STATUS_SWITCHING1 = 2;
const unsigned short STATUS_BACKWARD = 3;
const unsigned short STATUS_SWITCHING2 = 4;

const unsigned long BACKWARD_DURATION_MS = 2000;
const unsigned long SWITCHING_DURATION_MS = 1000;

const short MOTOR_STOPPED = 0;
const short MOTOR_RUNNING_FORWARD = 1;
const short MOTOR_RUNNING_BACKWARDS = -1;

const unsigned int LED_PIN = D0;
const unsigned int FORWARD_PIN = D1;
const unsigned int BACKWARD_PIN = D2;
const unsigned int BUTTON_PIN = D3;

unsigned short status = STATUS_READY;
short motor = MOTOR_STOPPED;
unsigned long lastSwitchedAt = 0;
volatile bool switchToBackward = false;

void setup() {
	Serial.begin(9600);
	Serial.setTimeout(2000);
	Serial.write("Setting up... \n");

	pinMode(FORWARD_PIN, OUTPUT);
	pinMode(BACKWARD_PIN, OUTPUT);
	setMotorStop();

	pinMode(BUTTON_PIN, INPUT_PULLUP);

	pinMode(LED_PIN, OUTPUT);
	for (int i = 0; i < 3; i++) {
		digitalWrite(LED_PIN, LOW);
		delay(150);
		digitalWrite(LED_PIN, HIGH);
		delay(150);
	}

	attachInterrupt(BUTTON_PIN, onButton, RISING);

	Serial.write("Setup finished.\n");
}



// The loop function is called in an endless loop
void loop() {
	//Serial.write("Loop started...\n");

	switch(status) {
	case STATUS_FORWARD:
		if (motor != MOTOR_RUNNING_FORWARD) {
			setMotorForward();
		}
		if (switchToBackward) {
			switchToBackward = false;
			status = STATUS_SWITCHING1;
		}
		break;
	case STATUS_SWITCHING1:
		if (motor != MOTOR_STOPPED) {
			setMotorStop();
		}
		if (millis() - lastSwitchedAt > SWITCHING_DURATION_MS) {
			status = STATUS_BACKWARD;
		}
		break;
	case STATUS_BACKWARD:
		if (motor != MOTOR_RUNNING_BACKWARDS) {
			setMotorBackward();
		}
		if (millis() - lastSwitchedAt > BACKWARD_DURATION_MS) {
			status = STATUS_SWITCHING2;
		}
		break;
	case STATUS_SWITCHING2:
		if (motor != MOTOR_STOPPED) {
			setMotorStop();
		}
		if (millis() - lastSwitchedAt > SWITCHING_DURATION_MS) {
			status = STATUS_FORWARD;
		}
		break;
	default:
		status = STATUS_FORWARD;
		break;
	}

	//Serial.write("Loop finished.\n");
}

void setMotorStop() {
	digitalWrite(FORWARD_PIN, LOW);
	digitalWrite(BACKWARD_PIN, LOW);
	digitalWrite(LED_PIN, HIGH);
	motor = MOTOR_STOPPED;
	lastSwitchedAt = millis();
	Serial.write("Motor stopped.\n");
}

void setMotorForward() {
	digitalWrite(FORWARD_PIN, HIGH);
	digitalWrite(BACKWARD_PIN, LOW);
	digitalWrite(LED_PIN, LOW);
	motor = MOTOR_RUNNING_FORWARD;
	lastSwitchedAt = millis();
	Serial.write("Motor running forward.\n");
}

void setMotorBackward() {
	digitalWrite(FORWARD_PIN, LOW);
	digitalWrite(BACKWARD_PIN, HIGH);
	digitalWrite(LED_PIN, LOW);
	motor = MOTOR_RUNNING_BACKWARDS;
	lastSwitchedAt = millis();
	Serial.write("Motor running backwards.\n");
}

void onButton() {
	Serial.write("* Button pressed!\n");
	if (status == STATUS_FORWARD && !switchToBackward) {
		switchToBackward = true;
	}
}
