#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-06-19 14:22:24

#include "Arduino.h"
#include <ESP8266WiFi.h>

void setup() ;
void loop() ;
void setMotorStop() ;
void setMotorForward() ;
void setMotorBackward() ;
void onButton() ;

#include "robocar.ino"


#endif
